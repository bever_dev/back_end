package ru.nsu.bever.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.nsu.bever.DTO.changeData.UserChangeDataDto;
import ru.nsu.bever.entity.User;
import ru.nsu.bever.exceptions.InvalidAuthenticationException;
import ru.nsu.bever.exceptions.UserAlreadyExistException;
import ru.nsu.bever.exceptions.UserNotFoundException;
import ru.nsu.bever.mapper.UserMapper;
import ru.nsu.bever.security.JwtTokenUtil;
import ru.nsu.bever.service.IUserService;
import ru.nsu.bever.util.UserRole;

import java.util.HashMap;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/authenticated")
public class UserController {
    private final IUserService userService;
    private final UserMapper userMapper;
    private final JwtTokenUtil tokenUtil;

    @ExceptionHandler({InvalidAuthenticationException.class})
    public ResponseEntity<?> exceptionHandle(InvalidAuthenticationException iae){
        return ResponseEntity.badRequest().body(iae.getMessage());
    }

    @Autowired
    public UserController(IUserService userService, UserMapper userMapper, JwtTokenUtil tokenUtil) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.tokenUtil = tokenUtil;
    }

    @GetMapping("/user")
    public ResponseEntity<?> getUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =  (UserDetails) authentication.getPrincipal();
        User user = userService.loadUserByNickname(userDetails.getUsername());

        return ResponseEntity
                .ok()
                .body(userMapper.toDTO(user));
    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<?> getUser(@PathVariable(value = "id") Long id){
        var user = userService.getUserById(id);
        return ResponseEntity
                .ok()
                .body(userMapper.toDTO(user));
    }

    private HashMap<String, Object> prepareResponseOfUpdatingUserData(User user){
        HashMap<String, Object> response = new HashMap<>();
        response.put("message", "User successfully modified!");
        response.put("jwtToken", tokenUtil.generateTokenOnlyByUsername(user.getNickname()));
        response.put("modifiedUser", userMapper.toDTO(user));

        return response;
    }

    @PutMapping(value = "/user")
    public ResponseEntity<?> updateAuthenticatedUser(@RequestBody UserChangeDataDto userChangeDataDto){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =  (UserDetails) authentication.getPrincipal();
        User user = userService.loadUserByNickname(userDetails.getUsername());

        var modifiedUser = userService.updateUser(user.getId(), userChangeDataDto);
        var response = prepareResponseOfUpdatingUserData(modifiedUser);

        return ResponseEntity
                .ok()
                .body(response);
    }

    @PutMapping(value = "/user/{id}")
    public ResponseEntity<?> updateUser(@PathVariable(value = "id") Long id, @RequestBody UserChangeDataDto userChangeDataDto){
        var modifiedUser = userService.updateUser(id, userChangeDataDto);
        var response = prepareResponseOfUpdatingUserData(modifiedUser);

        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping(value = "/users")
    public ResponseEntity<?> getUsers(){
        var user = userService.getUsersList().stream().map(userMapper::toDTO).collect(Collectors.toList());
        return ResponseEntity
                .ok()
                .body(user);
    }

    @DeleteMapping(value = "/user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long id){
        userService.deleteUser(id);
        return ResponseEntity
                .ok()
                .body("User deleted successfully!");
    }

}
