package ru.nsu.bever.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.nsu.bever.DTO.UserDto;
import ru.nsu.bever.entity.User;
import ru.nsu.bever.entity.roles.Role;
import ru.nsu.bever.exceptions.UserAlreadyExistException;
import ru.nsu.bever.mapper.UserMapper;
import ru.nsu.bever.security.JwtRequest;
import ru.nsu.bever.security.JwtResponse;
import ru.nsu.bever.security.JwtTokenUtil;
import ru.nsu.bever.service.UserService;

import java.util.stream.Collectors;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/auth")
public class AuthController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final JwtTokenUtil jwtTokenUtil;
    private final DaoAuthenticationProvider daoAuthenticationProvider;

    @Autowired
    public AuthController(UserService userService, UserMapper userMapper, JwtTokenUtil jwtTokenUtil,
                          DaoAuthenticationProvider daoAuthenticationProvider) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.jwtTokenUtil = jwtTokenUtil;
        this.daoAuthenticationProvider = daoAuthenticationProvider;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerNewUserAccount(@RequestBody final UserDto userDto){

        try {
            var user = userMapper.toEntity(userDto);
            userService.registerNewUserAccount(user);
        } catch (UserAlreadyExistException uaeEx) {
            return new ResponseEntity<String>("User is already registered! " + uaeEx.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity
                .ok()
                .body("User successfully created!");
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody JwtRequest jwtRequest){

        var authentication = daoAuthenticationProvider.authenticate(
                new UsernamePasswordAuthenticationToken(jwtRequest.getNickname(), jwtRequest.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails userDetails =  (UserDetails) authentication.getPrincipal();
        String jwt = jwtTokenUtil.generateToken(userDetails);
        User user = userService.loadUserByNickname(userDetails.getUsername());
        var userRoles = user.getRoles().stream().map(Role::getName).collect(Collectors.toList());

        return ResponseEntity
                .ok()
                .body(new JwtResponse(jwt, user.getId(), user.getNickname(), userDetails.getAuthorities(), userRoles));
    }

    @GetMapping("/token/validate")
    public ResponseEntity<?> validateToken(){
        return ResponseEntity
                .ok()
                .body(true);
    }
}
