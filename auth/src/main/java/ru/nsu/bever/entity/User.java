package ru.nsu.bever.entity;

import com.sun.istack.NotNull;
import jakarta.persistence.*;
import lombok.*;
import ru.nsu.bever.entity.roles.Role;
import ru.nsu.bever.util.Gender;

import java.util.Collection;
import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "sys_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NonNull
    private String nickname;
    @NonNull
    private String password;
    @NotNull
    private String email;
    @NonNull
    @Column(name = "first_name", length = 50)
    private String firstName;
    @Column(name = "surname", length = 50)
    private String surname;
    @NonNull
    @Column(name = "middle_name", length = 50)
    private String middleName;
    private Date dateBirth;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String profileImgUUID;
    @ManyToMany
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Collection<Role> roles;

    public User(@NonNull String nickname, @NonNull String password, String email, @NonNull String firstName,
                String surname, @NonNull String middleName, Gender gender) {
        this.nickname = nickname;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.surname = surname;
        this.middleName = middleName;
        this.gender = gender;
    }
}
