package ru.nsu.bever.util;

import lombok.Getter;

@Getter
public enum UserRole {
    ROLE_ADMIN("ADMIN"), ROLE_USER("USER");

    private String name;

    UserRole(String name) {
        this.name = name;
    }
}
