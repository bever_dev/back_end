package ru.nsu.bever.util;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.nsu.bever.entity.User;
import ru.nsu.bever.repository.PrivilegeRepository;
import ru.nsu.bever.entity.roles.Privilege;
import ru.nsu.bever.entity.roles.Role;
import ru.nsu.bever.repository.RoleRepository;
import ru.nsu.bever.repository.UserRepository;

import java.util.*;

@Component
@Slf4j
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    boolean alreadySetup = false;
    private final RoleRepository roleRepository;
    private final PrivilegeRepository privilegeRepository;
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public SetupDataLoader(RoleRepository roleRepository, PrivilegeRepository privilegeRepository, UserRepository userRepository,
                           PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.privilegeRepository = privilegeRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup)
            return;

        Privilege editLibraryPrivilege = createPrivilegeIfNotFound(UserPrivilege.EDIT_LIBRARY_PRIVILEGE.name());
        Privilege editMarketPrivilege = createPrivilegeIfNotFound(UserPrivilege.EDIT_MARKET_PRIVILEGE.name());

        createRoleIfNotFound(UserRole.ROLE_ADMIN.name(), Arrays.asList(editLibraryPrivilege, editMarketPrivilege));
        createRoleIfNotFound(UserRole.ROLE_USER.name(), null);
        createAdminForSystem();

        alreadySetup = true;
    }

    @Transactional
    private void createAdminForSystem() {
        if(userRepository.findUserByNickname("root") != null){
            return;
        }
        Role userRole = roleRepository.findByName(UserRole.ROLE_USER.name());
        Role adminRole = roleRepository.findByName(UserRole.ROLE_ADMIN.name());
        User user = new User();
        user.setNickname("root");
        user.setFirstName("Sergey");
        user.setSurname("Dovlatov");
        user.setEmail("s.dovlatov@gmail.com");
        user.setRoles(Arrays.asList(userRole, adminRole));
        user.setPassword(passwordEncoder.encode("test"));
        userRepository.save(user);
    }

    @Transactional
    private Privilege createPrivilegeIfNotFound(String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    private Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }

}
