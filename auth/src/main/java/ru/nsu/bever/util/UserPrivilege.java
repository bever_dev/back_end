package ru.nsu.bever.util;

public enum UserPrivilege {
    EDIT_LIBRARY_PRIVILEGE, EDIT_MARKET_PRIVILEGE
}
