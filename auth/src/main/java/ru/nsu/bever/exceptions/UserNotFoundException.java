package ru.nsu.bever.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="No user existing")
public final class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super();
    }
    public UserNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }
    public UserNotFoundException(final String message) {
        super(message);
    }
    public UserNotFoundException(final Throwable cause) {
        super(cause);
    }
}
