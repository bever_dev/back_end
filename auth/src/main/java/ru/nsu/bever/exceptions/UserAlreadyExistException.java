package ru.nsu.bever.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="User with such credentials is already exist")
public final class UserAlreadyExistException extends RuntimeException{
    public UserAlreadyExistException() {
        super();
    }
    public UserAlreadyExistException(final String message, final Throwable cause) {
        super(message, cause);
    }
    public UserAlreadyExistException(final String message) {
        super(message);
    }
    public UserAlreadyExistException(final Throwable cause) {
        super(cause);
    }

}
