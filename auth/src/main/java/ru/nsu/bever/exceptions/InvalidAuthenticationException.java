package ru.nsu.bever.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

public class InvalidAuthenticationException extends AuthenticationException {
    public InvalidAuthenticationException(String msg, Throwable cause) {
        super(msg, cause);
    }
    public InvalidAuthenticationException(String msg) {
        super(msg);
    }
}
