package ru.nsu.bever.DTO.changeData;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import ru.nsu.bever.util.Gender;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserChangeDataDto {
    private String nickname;
    private String firstName;
    private String middleName;
    private String surname;
    private String email;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private Date dateBirth;
    private String profileImgUUID;
    private String oldPassword;
    private String newPassword;
    private List<String> roles;
}
