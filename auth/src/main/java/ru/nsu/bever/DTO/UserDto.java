package ru.nsu.bever.DTO;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.bever.util.Gender;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements DTO{
    private Long id;
    private String nickname;
    private String firstName;
    private String middleName;
    private String surname;
    private String email;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String password;
    private String profileImgUUID;
    private List<String> roles;
}
