package ru.nsu.bever.security;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class JwtRequest implements Serializable {
    private String nickname;
    private String password;
}
