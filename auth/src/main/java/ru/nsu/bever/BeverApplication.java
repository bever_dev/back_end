package ru.nsu.bever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import ru.nsu.bever.configuration.ApplicationConfig;
import ru.nsu.bever.configuration.SecurityConfig;

@SpringBootApplication
@Import({ApplicationConfig.class, SecurityConfig.class})
public class BeverApplication {
    public static void main(String[] args) {
        SpringApplication.run(BeverApplication.class, args);
    }
}
