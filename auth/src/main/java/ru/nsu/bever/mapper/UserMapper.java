package ru.nsu.bever.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.bever.DTO.UserDto;
import ru.nsu.bever.entity.User;
import ru.nsu.bever.entity.roles.Role;

import java.util.stream.Collectors;

@Component
public class UserMapper implements Mapper<User, UserDto> {
    private final ModelMapper modelMapper;

    @Autowired
    public UserMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public User toEntity(UserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDto toDTO(User entity) {
        var userDto = modelMapper.map(entity, UserDto.class);
        userDto.setRoles(entity.getRoles().stream().map(Role::getName).collect(Collectors.toList()));

        return userDto;
    }
}
