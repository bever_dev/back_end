package ru.nsu.bever.mapper;

import ru.nsu.bever.DTO.DTO;

public interface Mapper<E, D extends DTO>{
    E toEntity(D dto);
    D toDTO(E entity);
}
