package ru.nsu.bever.service;

import ru.nsu.bever.DTO.changeData.UserChangeDataDto;
import ru.nsu.bever.entity.User;

import java.util.List;

public interface IUserService {
    User registerNewUserAccount(User user);
    User loadUserByNickname(String nickname);
    User getUserById(Long id);
    List<User> getUsersList();
    User updateUser(Long id, UserChangeDataDto userChangeDataDto);
    void deleteUser(Long id);
}
