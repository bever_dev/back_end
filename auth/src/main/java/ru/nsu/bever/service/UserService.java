package ru.nsu.bever.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nsu.bever.DTO.changeData.UserChangeDataDto;
import ru.nsu.bever.entity.User;
import ru.nsu.bever.exceptions.InvalidAuthenticationException;
import ru.nsu.bever.exceptions.UserAlreadyExistException;
import ru.nsu.bever.exceptions.UserNotFoundException;
import ru.nsu.bever.repository.RoleRepository;
import ru.nsu.bever.repository.UserRepository;
import ru.nsu.bever.util.UserRole;

import java.util.Collections;
import java.util.List;

@Service
public class UserService implements IUserService {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(RoleRepository roleRepository, UserRepository userRepository,
                       PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User registerNewUserAccount(User user) throws UserAlreadyExistException {
        if (nicknameExist(user.getNickname())) {
            throw new UserAlreadyExistException("User with specified nickname is already exist!");
        }

        if (emailExist(user.getEmail())) {
            throw new UserAlreadyExistException("User with specified email is already exist!");
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        var role = roleRepository.findByName(UserRole.ROLE_USER.name());
        user.setRoles(Collections.singletonList(role));

        return userRepository.save(user);
    }

    @Override
    public User loadUserByNickname(String nickname) {
        return userRepository.findUserByNickname(nickname);
    }

    @Override
    public User updateUser(Long id, UserChangeDataDto userChangeDataDto) {
        User existingUser = null;
        if(userRepository.findById(id).isPresent()){
            existingUser = userRepository.findById(id).get();
            existingUser.setNickname(userChangeDataDto.getNickname());
            existingUser.setFirstName(userChangeDataDto.getFirstName());
            existingUser.setSurname(userChangeDataDto.getSurname());
            existingUser.setMiddleName(userChangeDataDto.getMiddleName());
            existingUser.setDateBirth(userChangeDataDto.getDateBirth());
            existingUser.setEmail(userChangeDataDto.getEmail());
            existingUser.setGender(userChangeDataDto.getGender());
            if(userChangeDataDto.getProfileImgUUID() != null){
                existingUser.setProfileImgUUID(userChangeDataDto.getProfileImgUUID());
            }
            if(!userChangeDataDto.getNewPassword().equals("")){
                if(passwordEncoder.matches(userChangeDataDto.getOldPassword(), existingUser.getPassword())){
                    existingUser.setPassword(passwordEncoder.encode(userChangeDataDto.getNewPassword()));
                }  else {
                    throw new InvalidAuthenticationException("Incorrect old password");
                }
            }
        } else {
            throw new UserNotFoundException();
        }

        return userRepository.save(existingUser);
    }

    @Override
    public User getUserById(Long id) {
        var user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new UserNotFoundException();
        }
        return user.get();
    }

    @Override
    public List<User> getUsersList() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.delete(getUserById(id));
    }

    private boolean emailExist(String email) {
        return userRepository.findUserByEmail(email) != null;
    }

    private boolean nicknameExist(String nickname) {
        return userRepository.findUserByNickname(nickname) != null;
    }
}
