package ru.nsu.bever;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.nsu.bever.DTO.changeData.UserChangeDataDto;
import ru.nsu.bever.entity.User;
import ru.nsu.bever.entity.roles.Role;
import ru.nsu.bever.exceptions.UserAlreadyExistException;
import ru.nsu.bever.repository.RoleRepository;
import ru.nsu.bever.repository.UserRepository;
import ru.nsu.bever.service.UserService;
import ru.nsu.bever.util.Gender;
import ru.nsu.bever.util.UserRole;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        userService = new UserService(roleRepository, userRepository, passwordEncoder);
    }

    @Test
    public void whenRegisterUser_thenCheckData(){
        var user = new User("admin", "password", "admin@mail.com",
                "Ivan", "Ivanov", "Ivanovich", Gender.MALE);
        var role = new Role(UserRole.ROLE_USER.name());
        Mockito.when(userRepository.findUserByEmail(user.getEmail())).thenReturn(null);
        Mockito.when(userRepository.findUserByNickname(user.getNickname())).thenReturn(null);
        Mockito.when(roleRepository.findByName(UserRole.ROLE_USER.name())).thenReturn(role);

        userService.registerNewUserAccount(user);
        Assertions.assertTrue(passwordEncoder.matches("password", user.getPassword()));
        Assertions.assertTrue(user.getRoles().contains(role));
    }

    @Test
    public void whenLoadUserByNickname_thenCheckData(){
        var user = new User("admin", "password", "admin@mail.com",
                "Ivan", "Ivanov", "Ivanovich", Gender.MALE);

        Mockito.lenient().when(userRepository.findUserByNickname(user.getNickname())).thenReturn(user);

        var loadedUser = userService.loadUserByNickname(user.getNickname());
        Assertions.assertTrue(CoreMatchers.is(user.getPassword()).matches(loadedUser.getPassword()));
        Assertions.assertTrue(CoreMatchers.is(user.getNickname()).matches(loadedUser.getNickname()));
        Assertions.assertTrue(CoreMatchers.is(user.getFirstName()).matches(loadedUser.getFirstName()));
        Assertions.assertTrue(CoreMatchers.is(user.getSurname()).matches(loadedUser.getSurname()));
        Assertions.assertTrue(CoreMatchers.is(user.getMiddleName()).matches(loadedUser.getMiddleName()));
        Assertions.assertTrue(CoreMatchers.is(user.getGender()).matches(loadedUser.getGender()));
    }

    @Test
    public void whenRegisterUserWithExistingEmail_thenCatchException(){
        var user = new User("admin", "password", "admin@mail.com",
                "Ivan", "Ivanov", "Ivanovich", Gender.MALE);
        Mockito.when(userRepository.findUserByEmail(user.getEmail())).thenReturn(user);
        Assertions.assertThrows(UserAlreadyExistException.class, () -> userService.registerNewUserAccount(user));
    }

    @Test
    public void whenRegisterUserWithExistingNickname_thenCatchException(){
        var user = new User("admin", "password", "admin@mail.com",
                "Ivan", "Ivanov", "Ivanovich", Gender.MALE);
        Mockito.when(userRepository.findUserByNickname(user.getNickname())).thenReturn(user);
        Assertions.assertThrows(UserAlreadyExistException.class, () -> userService.registerNewUserAccount(user));
    }

    @Test
    public void whenUpdateUser_thenCheckData(){
        var userChangeDataDto = new UserChangeDataDto("admin", "Ivan", "Ivanovich", "Ivanov",
                "ivanov@mail.com", Gender.MALE, null, null, "oldpassword", "newpassword", null);
        var modifiedUser = new User("admin", "newpassword", "ivanov@mail.com",
                "Ivan", "Ivanov", "Ivanovich", Gender.MALE);
        var user = new User("antonsmith", passwordEncoder.encode("oldpassword"), "admin@mail.com",
                "Anton", "Smith", "Pavlovich", Gender.MALE);
        Long id = 0L;
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.save(user)).thenReturn(user);
        var result = userService.updateUser(0L, userChangeDataDto);
        Assertions.assertTrue(passwordEncoder.matches(modifiedUser.getPassword(), result.getPassword()));
        Assertions.assertEquals(modifiedUser.getNickname(), result.getNickname());
        Assertions.assertEquals(modifiedUser.getFirstName(), result.getFirstName());
        Assertions.assertEquals(modifiedUser.getSurname(), result.getSurname());
        Assertions.assertEquals(modifiedUser.getMiddleName(), result.getMiddleName());
        Assertions.assertEquals(modifiedUser.getGender(), result.getGender());
        Assertions.assertEquals(modifiedUser.getEmail(), result.getEmail());
    }

    @Test
    public void whenGetUserById_thenCheckData(){
        var user = new User("antonsmith", passwordEncoder.encode("oldpassword"), "admin@mail.com",
                "Anton", "Smith", "Pavlovich", Gender.MALE);
        Long id = 0L;
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        var result = userService.getUserById(id);

        Assertions.assertTrue(passwordEncoder.matches("oldpassword", result.getPassword()));
        Assertions.assertEquals(user.getNickname(), result.getNickname());
        Assertions.assertEquals(user.getFirstName(), result.getFirstName());
        Assertions.assertEquals(user.getSurname(), result.getSurname());
        Assertions.assertEquals(user.getMiddleName(), result.getMiddleName());
        Assertions.assertEquals(user.getGender(), result.getGender());
        Assertions.assertEquals(user.getEmail(), result.getEmail());
    }
}
