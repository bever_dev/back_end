package ru.bever.prefer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bever.prefer.entity.Favorite;
import ru.bever.prefer.exception.FavItemAlreadyExistException;
import ru.bever.prefer.exception.FavItemNotFoundException;
import ru.bever.prefer.repository.FavoriteRepository;

import java.util.List;

@Service
public class FavoriteService implements IFavoriteService{

    private final FavoriteRepository favoriteRepository;

    @Autowired
    public FavoriteService(FavoriteRepository favoriteRepository) {
        this.favoriteRepository = favoriteRepository;
    }

    @Override
    public Favorite saveFavorite(Long userId, Long beverageId){
        var favorite = favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId);
        if(favorite != null){
            throw new FavItemAlreadyExistException();
        }
        return favoriteRepository.save(new Favorite(userId, beverageId));
    }

    @Override
    public void deleteFavorite(Long favoriteId) {
        var favorite = favoriteRepository.findById(favoriteId);
        if(favorite.isEmpty()){
            throw new FavItemNotFoundException();
        }
        favoriteRepository.delete(favorite.get());
    }

    @Override
    public void deleteFavoritesByUserId(Long userId) {
        favoriteRepository.deleteFavoriteByUserId(userId);
    }

    @Override
    public void deleteFavoritesByBeverageId(Long beverageId) {
        favoriteRepository.deleteFavoriteByBeverageId(beverageId);
    }

    @Override
    public List<Favorite> getFavoritesByUserId(Long userId) {
        return favoriteRepository.getFavoriteByUserId(userId);
    }

    @Override
    public List<Favorite> getFavoritesByBeverageId(Long beverageId) {
        return favoriteRepository.getFavoriteByBeverageId(beverageId);
    }

    @Override
    public boolean isBeverageInUserFavorite(Long beverageId, Long userId) {
        return favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId) != null;
    }

    @Override
    public void deleteFavoritesByUserIdAndBeverageId(Long userId, Long beverageId) {
        var favorite = favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId);
        if(favorite == null){
            throw new FavItemNotFoundException();
        }
        favoriteRepository.delete(favorite);
    }
}
