package ru.bever.prefer.service;

import ru.bever.prefer.entity.Favorite;

import java.util.List;

public interface IFavoriteService {
    Favorite saveFavorite(Long userId, Long beverageId);
    void deleteFavorite(Long favoriteId);
    void deleteFavoritesByUserId(Long userId);
    void deleteFavoritesByBeverageId(Long beverageId);
    void deleteFavoritesByUserIdAndBeverageId(Long userId, Long beverageId);
    List<Favorite> getFavoritesByUserId(Long userId);
    List<Favorite> getFavoritesByBeverageId(Long beverageId);
    boolean isBeverageInUserFavorite(Long beverageId, Long userId);
}
