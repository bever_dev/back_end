package ru.bever.prefer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bever.prefer.controller.request.FavoriteRequest;
import ru.bever.prefer.entity.Favorite;
import ru.bever.prefer.exception.FavItemAlreadyExistException;
import ru.bever.prefer.exception.FavItemNotFoundException;
import ru.bever.prefer.service.IFavoriteService;

import javax.servlet.http.HttpServletRequest;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/prefer")
public class FavoriteController {

    private final IFavoriteService favoriteService;

    @Autowired
    public FavoriteController(IFavoriteService favoriteService) {
        this.favoriteService = favoriteService;
    }

    @ExceptionHandler({NumberFormatException.class})
    public ResponseEntity<?> exceptionHandle(NumberFormatException e){
        return ResponseEntity.badRequest().body("Invalid ids passed!");
    }

    @ExceptionHandler({InvalidParameterException.class})
    public ResponseEntity<?> exceptionHandle(InvalidParameterException e){
        return ResponseEntity.badRequest().body("Invalid parameter passed!");
    }

    @ExceptionHandler({FavItemAlreadyExistException.class})
    public ResponseEntity<?> exceptionHandle(FavItemAlreadyExistException e){
        return ResponseEntity.badRequest().body("Beverage is already in user's favorite!");
    }

    @ExceptionHandler({FavItemNotFoundException.class})
    public ResponseEntity<?> exceptionHandle(FavItemNotFoundException e){
        return ResponseEntity.badRequest().body("There is no favorite item with such params!");
    }


    @PostMapping("/favorite")
    public ResponseEntity<?> addFavoriteToUser(@RequestBody FavoriteRequest request){
        if(request.getUserId() == null || request.getBeverageId() == null){
            return ResponseEntity.badRequest().body("Invalid add favorite request");
        }
        var favorite = favoriteService.saveFavorite(request.getUserId(), request.getBeverageId());
        return ResponseEntity
                .ok()
                .body(favorite);
    }

    private String getValidatedParam(HttpServletRequest request, String fieldName){
        var param = request.getParameter(fieldName);
        if(param == null) {
            throw new InvalidParameterException();
        }
        return param;
    }

    private HashMap<String, Object> wrapFavorites(List<Favorite> favorites){
        HashMap<String, Object> response = new HashMap<>();
        response.put("count", favorites.size());
        response.put("favorites", favorites);
        return response;
    }

    @GetMapping("/favorites/user")
    public ResponseEntity<?> getFavoritesByUserId(HttpServletRequest request){
        var userId = Long.parseLong(getValidatedParam(request, "userId"));
        var favorites = favoriteService.getFavoritesByUserId(userId);
        return ResponseEntity
                .ok()
                .body(wrapFavorites(favorites));
    }

    @GetMapping("/favorites/beverage")
    public ResponseEntity<?> getFavoritesByBeverageId(HttpServletRequest request){
        var beverageId = Long.parseLong(getValidatedParam(request, "beverageId"));
        var favorites = favoriteService.getFavoritesByBeverageId(beverageId);
        return ResponseEntity
                .ok()
                .body(wrapFavorites(favorites));
    }

    @GetMapping("/favorites/check")
    public ResponseEntity<?> checkIfBeverageInUserFavorites(HttpServletRequest request){
        var beverageId = Long.parseLong(getValidatedParam(request, "beverageId"));
        var userId = Long.parseLong(getValidatedParam(request, "userId"));
        var result = favoriteService.isBeverageInUserFavorite(beverageId, userId);
        return ResponseEntity
                .ok()
                .body(result);
    }

    @DeleteMapping("/favorite")
    public ResponseEntity<?> deleteFavorite(HttpServletRequest request){
        var userId = Long.parseLong(getValidatedParam(request, "userId"));
        var beverageId = Long.parseLong(getValidatedParam(request, "beverageId"));
        favoriteService.deleteFavoritesByUserIdAndBeverageId(userId, beverageId);
        return ResponseEntity
                .ok()
                .body("Favorite which are connected with beverage id: {" + beverageId + "}" +
                        "and user id: {" + userId + "} - successfully deleted!");
    }

    @DeleteMapping("/favorites/user")
    public ResponseEntity<?> deleteFavoriteByUserId(HttpServletRequest request){
        var userId = Long.parseLong(getValidatedParam(request, "userId"));
        favoriteService.deleteFavoritesByUserId(userId);
        return ResponseEntity
                .ok()
                .body("Favorite which are connected with user id: {" + userId + "} - successfully deleted!");
    }

    @DeleteMapping("/favorites/beverage")
    public ResponseEntity<?> deleteFavoriteByBeverageId(HttpServletRequest request){
        var beverageId = Long.parseLong(getValidatedParam(request, "beverageId"));
        favoriteService.deleteFavoritesByBeverageId(beverageId);
        return ResponseEntity
                .ok()
                .body("Favorite which are connected with beverage id: {" + beverageId + "} - successfully deleted!");
    }
}
