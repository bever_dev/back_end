package ru.bever.prefer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreferApplication {

    public static void main(String[] args) {
        SpringApplication.run(PreferApplication.class, args);
    }

}
