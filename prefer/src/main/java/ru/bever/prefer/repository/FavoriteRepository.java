package ru.bever.prefer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.bever.prefer.entity.Favorite;

import java.util.List;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Long> {
    List<Favorite> getFavoriteByUserId(Long userId);
    List<Favorite> getFavoriteByBeverageId(Long beverageId);
    Favorite findFavoriteByUserIdAndBeverageId(Long userId, Long beverageId);
    @Transactional
    void deleteFavoriteByBeverageId(Long beverageId);
    @Transactional
    void deleteFavoriteByUserId(Long userId);
}
