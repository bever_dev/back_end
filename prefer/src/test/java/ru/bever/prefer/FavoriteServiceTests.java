package ru.bever.prefer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.bever.prefer.entity.Favorite;
import ru.bever.prefer.exception.FavItemAlreadyExistException;
import ru.bever.prefer.exception.FavItemNotFoundException;
import ru.bever.prefer.repository.FavoriteRepository;
import ru.bever.prefer.service.FavoriteService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;

@ExtendWith(MockitoExtension.class)
public class FavoriteServiceTests {

    @Mock
    private FavoriteRepository favoriteRepository;

    @InjectMocks
    private FavoriteService favoriteService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        favoriteService = new FavoriteService(favoriteRepository);
    }

    @Test
    public void whenAddNotExistingFavorite_thenCheckData(){
        Long userId = 12L;
        Long beverageId = 21L;
        var favorite = new Favorite(userId, beverageId);
        Mockito.when(favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId)).thenReturn(null);
        Mockito.when(favoriteRepository.save(Mockito.any(Favorite.class))).thenReturn(favorite);
        var result = favoriteService.saveFavorite(userId, beverageId);
        assertEquals(favorite.getUserId(), result.getUserId());
        assertEquals(favorite.getBeverageId(), result.getBeverageId());
    }

    @Test
    public void whenAddExistingFavorite_thenCheckData(){
        Long userId = 12L;
        Long beverageId = 21L;
        var favorite = new Favorite(userId, beverageId);
        Mockito.when(favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId)).thenReturn(favorite);
        Assertions.assertThrows(FavItemAlreadyExistException.class, () -> favoriteService.saveFavorite(userId, beverageId));
    }

    @Test
    public void whenGetFavoritesByUserId_chenCheckData(){
        Long userId = 12L;
        var favorites = new ArrayList<>(
                Arrays.asList(
                        new Favorite(userId, 23L),
                        new Favorite(userId, 24L),
                        new Favorite(userId, 25L))
        );
        Mockito.when(favoriteRepository.getFavoriteByUserId(userId)).thenReturn(favorites);
        var result = favoriteService.getFavoritesByUserId(userId);
        assertEquals(result.size(), 3);
    }

    @Test
    public void whenGetFavoritesByBeverageId_chenCheckData(){
        Long beverageId = 21L;
        var favorites = new ArrayList<>(
                Arrays.asList(
                        new Favorite(12L, beverageId),
                        new Favorite(13L, beverageId),
                        new Favorite(14L, beverageId))
        );
        Mockito.when(favoriteRepository.getFavoriteByBeverageId(beverageId)).thenReturn(favorites);
        var result = favoriteService.getFavoritesByBeverageId(beverageId);
        assertEquals(result.size(), 3);
    }

    @Test
    public void whenDeleteNotExistingFavoritesById_chenCheckData(){
        Long favoriteId = 11L;
        Mockito.when(favoriteRepository.findById(favoriteId)).thenReturn(Optional.empty());
        Assertions.assertThrows(FavItemNotFoundException.class, () -> favoriteService.deleteFavorite(favoriteId));
    }

    @Test
    public void whenCheckIfBeverageInUserFavorite_thenCheckData(){
        Long userId = 12L;
        Long beverageId = 21L;
        var favorite = new Favorite(userId, beverageId);
        Mockito.when(favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId)).thenReturn(favorite);
        var result = favoriteService.isBeverageInUserFavorite(beverageId, userId);
        Assertions.assertTrue(result);
    }

    @Test
    public void whenDeleteNotExistingFavoriteByUserIdAndBeverageId_chenCheckData(){
        Long userId = 11L;
        Long beverageId = 12L;
        Mockito.when(favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId)).thenReturn(null);
        Assertions.assertThrows(FavItemNotFoundException.class,
                () -> favoriteService.deleteFavoritesByUserIdAndBeverageId(userId, beverageId));
    }

    @Test
    public void whenDeleteFavoriteByUserIdAndBeverageId(){
        Long userId = 11L;
        Long beverageId = 12L;
        var favorite = new Favorite(userId, beverageId);
        Mockito.when(favoriteRepository.findFavoriteByUserIdAndBeverageId(userId, beverageId)).thenReturn(favorite);
        doAnswer(invocation -> {
            var givenFav = (Favorite) invocation.getArgument(0);
            assertEquals(userId, givenFav.getUserId());
            assertEquals(beverageId, givenFav.getBeverageId());
            return null;
        }).when(favoriteRepository).delete(favorite);

        favoriteService.deleteFavoritesByUserIdAndBeverageId(userId, beverageId);
    }

    @Test
    public void whenDeleteFavoriteByFavoriteId(){
        Long userId = 11L;
        Long beverageId = 12L;
        Long favoriteId = 1L;
        var favorite = new Favorite(userId, beverageId);
        Mockito.when(favoriteRepository.findById(favoriteId)).thenReturn(Optional.of(favorite));
        doAnswer(invocation -> {
            var givenFav = (Favorite) invocation.getArgument(0);
            assertEquals(userId, givenFav.getUserId());
            assertEquals(beverageId, givenFav.getBeverageId());
            return null;
        }).when(favoriteRepository).delete(favorite);

        favoriteService.deleteFavorite(favoriteId);
    }

    @Test
    public void whenDeleteFavoritesByUserId(){
        Long userId = 11L;
        Long beverageId = 12L;
        doAnswer(invocation -> {
            Long givenUserId = invocation.getArgument(0);
            assertEquals(userId, givenUserId);
            return null;
        }).when(favoriteRepository).deleteFavoriteByUserId(userId);

        favoriteService.deleteFavoritesByUserId(userId);
    }

    @Test
    public void whenDeleteFavoritesByBeverageId(){
        Long beverageId = 12L;
        doAnswer(invocation -> {
            Long givenBevId = invocation.getArgument(0);
            assertEquals(beverageId, givenBevId);
            return null;
        }).when(favoriteRepository).deleteFavoriteByBeverageId(beverageId);

        favoriteService.deleteFavoritesByBeverageId(beverageId);
    }

}
