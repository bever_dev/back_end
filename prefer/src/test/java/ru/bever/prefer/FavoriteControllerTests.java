package ru.bever.prefer;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.bever.prefer.controller.FavoriteController;
import ru.bever.prefer.controller.request.FavoriteRequest;
import ru.bever.prefer.entity.Favorite;
import ru.bever.prefer.exception.FavItemAlreadyExistException;
import ru.bever.prefer.exception.FavItemNotFoundException;
import ru.bever.prefer.service.IFavoriteService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@AutoConfigureJsonTesters
@WebMvcTest(FavoriteController.class)
public class FavoriteControllerTests {

    @MockBean
    private IFavoriteService favoriteService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JacksonTester<FavoriteRequest> jsonSingleConverterFavReq;
    @Autowired
    private JacksonTester<Favorite> jsonSingleConverterFav;
    @Autowired
    private JacksonTester<HashMap<String, Object>> jsonSingleConverterObj;
    @Autowired
    private JacksonTester<List<Favorite>> jsonMultipleConverterFav;

    @Test
    void postValidFavorite() throws Exception {
        Long userId = 11L;
        Long beverageId = 12L;
        var favoriteRequest = new FavoriteRequest(userId, beverageId);
        var expectedFavorite = new Favorite(userId, beverageId);

        BDDMockito.given(favoriteService.saveFavorite(userId, beverageId)).willReturn(expectedFavorite);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/prefer/favorite")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonSingleConverterFavReq.write(favoriteRequest).getJson()))
                .andReturn()
                .getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo(jsonSingleConverterFav.write(expectedFavorite).getJson());
    }

    @Test
    void postValidFavorite_alreadyExisted() throws Exception {
        Long userId = 11L;
        Long beverageId = 12L;
        var favoriteRequest = new FavoriteRequest(userId, beverageId);

        BDDMockito.given(favoriteService.saveFavorite(userId, beverageId)).willThrow(FavItemAlreadyExistException.class);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/prefer/favorite")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSingleConverterFavReq.write(favoriteRequest).getJson()))
                .andReturn()
                .getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo("Beverage is already in user's favorite!");
    }

    @Test
    void deleteExistedFavoriteByUserAndBeverage() throws Exception {
        Long userId = 11L;
        Long beverageId = 12L;

        var response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/prefer/favorite")
                        .param("userId", String.valueOf(userId))
                        .param("beverageId", String.valueOf(beverageId)))
                .andReturn()
                .getResponse();

        favoriteService.deleteFavoritesByUserIdAndBeverageId(userId, beverageId);
        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void deleteExistedFavoritesByUser() throws Exception {
        Long userId = 11L;

        var response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/prefer/favorites/user")
                        .param("userId", String.valueOf(userId)))
                .andReturn()
                .getResponse();

        favoriteService.deleteFavoritesByUserId(userId);
        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void deleteExistedFavoritesByBeverage() throws Exception {
        Long beverageId = 11L;

        var response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/prefer/favorites/beverage")
                        .param("beverageId", String.valueOf(beverageId)))
                .andReturn()
                .getResponse();

        favoriteService.deleteFavoritesByBeverageId(beverageId);
        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void checkBeverageInUserFavorites() throws Exception {
        Long userId = 11L;
        Long beverageId = 12L;

        BDDMockito.given(favoriteService.isBeverageInUserFavorite(beverageId, userId)).willReturn(true);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/prefer/favorites/check")
                        .param("beverageId", String.valueOf(beverageId))
                        .param("userId", String.valueOf(userId)))
                .andReturn()
                .getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo("true");
    }

    @Test
    void checkBeverageInUserFavorites_invalidParams() throws Exception {
        Long userId = 11L;
        Long beverageId = 12L;

        BDDMockito.given(favoriteService.isBeverageInUserFavorite(beverageId, userId)).willReturn(true);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/prefer/favorites/check")
                        .param("beverageId", String.valueOf(beverageId))
                        .param("userId", "userId"))
                .andReturn()
                .getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo("Invalid ids passed!");
    }

    @Test
    void getFavoritesByUser() throws Exception {
        Long userId = 11L;

        var expectedFavs = Arrays.asList(
                new Favorite(userId, 12L), new Favorite(userId, 13L),
                new Favorite(userId, 14L), new Favorite(userId, 15L));

        BDDMockito.given(favoriteService.getFavoritesByUserId(userId)).willReturn(expectedFavs);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/prefer/favorites/user")
                        .param("userId", String.valueOf(userId)))
                .andReturn()
                .getResponse();

        HashMap<String, Object> expectedResponse = new HashMap<>();
        expectedResponse.put("favorites", expectedFavs);
        expectedResponse.put("count", 4);

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString())
                .isEqualTo(jsonSingleConverterObj.write(expectedResponse).getJson());
    }

    @Test
    void getFavoritesByUser_invalidUserId() throws Exception {
        Long userId = 11L;

        BDDMockito.given(favoriteService.getFavoritesByUserId(userId)).willThrow(FavItemNotFoundException.class);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/prefer/favorites/user")
                        .param("userId", String.valueOf(userId)))
                .andReturn()
                .getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        BDDAssertions.then(response.getContentAsString())
                .isEqualTo("There is no favorite item with such params!");
    }

    @Test
    void getFavoritesByBeverage() throws Exception {
        Long beverageId = 12L;

        var expectedFavs = Arrays.asList(
                new Favorite(11L, beverageId), new Favorite(12L, beverageId),
                new Favorite(13L, beverageId), new Favorite(14L, beverageId));

        BDDMockito.given(favoriteService.getFavoritesByBeverageId(beverageId)).willReturn(expectedFavs);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/prefer/favorites/beverage")
                        .param("beverageId", String.valueOf(beverageId)))
                .andReturn()
                .getResponse();

        HashMap<String, Object> expectedResponse = new HashMap<>();
        expectedResponse.put("favorites", expectedFavs);
        expectedResponse.put("count", 4);

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString())
                .isEqualTo(jsonSingleConverterObj.write(expectedResponse).getJson());
    }

}

