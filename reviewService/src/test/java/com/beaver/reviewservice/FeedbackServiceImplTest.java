package com.beaver.reviewservice;

import com.beaver.reviewservice.entity.Feedback;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.AdditionalAnswers.returnsFirstArg;

@ExtendWith(MockitoExtension.class)
class FeedbackServiceImplTest {
    private FeedbackService feedbackService;

    @Mock
    private FeedbackRepository feedbackRepository;

    @BeforeEach
    void setUp() {
        feedbackService = new FeedbackServiceImpl(feedbackRepository);
    }

    @Test
    void addCorrectFeedback() {
        Feedback feedback = new Feedback(null, 1L, "Ruslan", "Some feedback", 3, null, null);
        Feedback expectedFeedback = new Feedback(null, 1L, "Ruslan", "Some feedback", 3, FeedbackStatus.ON_REVIEW, null);
        BDDMockito.given(feedbackRepository.save(feedback)).will(returnsFirstArg());

        BDDAssertions.then(feedbackService.addNewFeedbackForReview(feedback)).isEqualTo(expectedFeedback);
        BDDMockito.verify(feedbackRepository).save(feedback);
    }

    @Test
    void addIncorrectFeedback() {
        Feedback feedbackWithEmptyUser = new Feedback(null, 1L, "", "Some feedback", 3, null, null);
        Feedback feedbackWithEmptyFeedbackBody = new Feedback(null, 2L, "Ruslan", "", 3, null, null);

        BDDAssertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> feedbackService.addNewFeedbackForReview(feedbackWithEmptyUser))
                .withMessage("Feedback username cannot be empty!");
        BDDAssertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> feedbackService.addNewFeedbackForReview(feedbackWithEmptyFeedbackBody))
                .withMessage("Feedback body cannot be empty!");

        BDDMockito.verify(feedbackRepository, Mockito.never()).save(feedbackWithEmptyUser);
        BDDMockito.verify(feedbackRepository, Mockito.never()).save(feedbackWithEmptyFeedbackBody);
    }

    @Test
    void deleteFeedbackWithNonExistId() {
        BDDMockito.given(feedbackRepository.existsById(BDDMockito.anyLong())).willReturn(false);

        BDDAssertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> feedbackService.deleteFeedbackByFeedbackId(1L))
                .withMessage("Feedback with given id doesn't exist");
    }

    @Test
    void deleteExistingFeedback() {
        BDDMockito.given(feedbackRepository.existsById(BDDMockito.anyLong())).willReturn(true);

        feedbackService.deleteFeedbackByFeedbackId(1L);

        BDDMockito.verify(feedbackRepository).deleteFeedbackByFeedbackId(1L);
    }

    @Test
    void getAllUncheckedReviews() {
        Feedback feedback1 = new Feedback(1L, 1L, "Ruslan", "Some body1", 3, FeedbackStatus.ON_REVIEW, null);
        Feedback feedback2 = new Feedback(2L, 1L, "Roma", "Some body2", 4, FeedbackStatus.ON_REVIEW, null);
        Feedback feedback3 = new Feedback(3L, 1L, "Misha", "Some body3", 5, FeedbackStatus.ON_REVIEW, null);

        List<Feedback> uncheckedFeedbacks = Arrays.asList(feedback1, feedback2, feedback3);

        BDDMockito.given(feedbackRepository.getFeedbacksByFeedbackStatusIsOnReview()).willReturn(uncheckedFeedbacks);

        BDDAssertions.assertThat(uncheckedFeedbacks.equals(feedbackService.getAllUncheckedFeedbacks())).isTrue();
    }

    @Test
    void getAllConfirmedReviews() {
        Feedback feedback1 = new Feedback(1L, 1L, "Ruslan", "Some body1", 3, FeedbackStatus.CONFIRMED, null);
        Feedback feedback2 = new Feedback(2L, 1L, "Roma", "Some body2", 4, FeedbackStatus.CONFIRMED, null);
        Feedback feedback3 = new Feedback(3L, 1L, "Misha", "Some body3", 5, FeedbackStatus.CONFIRMED, null);

        List<Feedback> confirmedFeedbacks = Arrays.asList(feedback1, feedback2, feedback3);

        BDDMockito.given(feedbackRepository.getConfirmedFeedbacksByBeverageId(BDDMockito.anyLong())).willReturn(confirmedFeedbacks);

        BDDAssertions.assertThat(confirmedFeedbacks.equals(feedbackService.getAllConfirmedFeedbacksForBeverage(1L))).isTrue();
    }

    @Test
    void updateNonExistFeedbackStatus() {
        Feedback nonExistFeedback = new Feedback(1L, 1L, "John", "Strange...", 2, FeedbackStatus.ON_REVIEW, null);

        BDDMockito.given(feedbackRepository.existsById(BDDMockito.anyLong())).willReturn(false);

        BDDAssertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> feedbackService.setFeedbackStatusIsConfirmed(nonExistFeedback))
                .withMessage("Feedback with given id doesn't exist");
    }

    @Test
    void updateExistingFeedbackStatus() {
        Feedback existFeedback = new Feedback(1L, 1L, "John", "Strange...", 2, FeedbackStatus.ON_REVIEW, null);

        BDDMockito.given(feedbackRepository.existsById(BDDMockito.anyLong())).willReturn(true);

        feedbackService.setFeedbackStatusIsConfirmed(existFeedback);

        BDDMockito.verify(feedbackRepository).updateStatusToConfirmedForFeedback(existFeedback.getFeedbackId());
    }
}
