package com.beaver.reviewservice;

import com.beaver.reviewservice.entity.Feedback;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@AutoConfigureJsonTesters
@WebMvcTest(FeedbackController.class)
public class FeedbackControllerTest {
    @MockBean
    private FeedbackService feedbackService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JacksonTester<Feedback> jsonSingleConverter;
    @Autowired
    private JacksonTester<List<Feedback>> jsonMultipleConverter;

    @Test
    void postValidReview() throws Exception {
        Feedback feedback = new Feedback(1L, 1L, "John", "Liked it!", 3, FeedbackStatus.ON_REVIEW, null);
        Feedback expectedFeedback = new Feedback();
        BeanUtils.copyProperties(feedback, expectedFeedback);

        BDDMockito.given(feedbackService.addNewFeedbackForReview(ArgumentMatchers.eq(feedback))).willReturn(expectedFeedback);

        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.post("/api/lib/feedback")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSingleConverter.write(feedback)
                                .getJson()))
                .andReturn().getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo(jsonSingleConverter.write(feedback).getJson());
    }

    @Test
    void postConfirmedReview() throws Exception {
        Feedback unapprovedFeedback = new Feedback(1L, 1L, "John", "Bad juice!!", 2, FeedbackStatus.ON_REVIEW, null);
        Feedback approvedFeedback = new Feedback();
        BeanUtils.copyProperties(unapprovedFeedback, approvedFeedback);
        approvedFeedback.setFeedbackStatus(FeedbackStatus.CONFIRMED);

        BDDMockito.given(feedbackService.setFeedbackStatusIsConfirmed(ArgumentMatchers.eq(unapprovedFeedback))).willReturn(approvedFeedback);

        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.put("/api/lib/feedback/approve")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSingleConverter.write(unapprovedFeedback)
                                .getJson()))
                .andReturn().getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo(jsonSingleConverter.write(approvedFeedback).getJson());
    }

    @Test
    void deleteReview() throws Exception {
        Feedback deletedFeedback = new Feedback(1L, 1L, "John", "Bad juice!!", 2, FeedbackStatus.CONFIRMED, null);

        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.post("/api/lib/feedback/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSingleConverter.write(deletedFeedback).getJson()))
                .andReturn().getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo(jsonSingleConverter.write(deletedFeedback).getJson());
    }

    @Test
    void getFeedbacksForBeverage() throws Exception {
        Feedback feedback1 = new Feedback(1L, 1L, "Ruslan", "Some body1", 3, FeedbackStatus.CONFIRMED, null);
        Feedback feedback2 = new Feedback(2L, 1L, "Roma", "Some body2", 4, FeedbackStatus.CONFIRMED, null);
        Feedback feedback3 = new Feedback(3L, 1L, "Misha", "Some body3", 5, FeedbackStatus.CONFIRMED, null);
        List<Feedback> confirmedFeedbacks = Arrays.asList(feedback1, feedback2, feedback3);

        BDDMockito.given(feedbackService.getAllConfirmedFeedbacksForBeverage(1L)).willReturn(confirmedFeedbacks);

        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.get("/api/lib/feedback").param("beverageId", feedback1.getBeverageId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo(jsonMultipleConverter.write(confirmedFeedbacks).getJson());
    }

    @Test
    void getFeedbacksOnReview() throws Exception {
        Feedback feedback1 = new Feedback(1L, 1L, "Ruslan", "Some body1", 3, FeedbackStatus.ON_REVIEW, null);
        Feedback feedback2 = new Feedback(2L, 1L, "Roma", "Some body2", 4, FeedbackStatus.ON_REVIEW, null);
        Feedback feedback3 = new Feedback(3L, 1L, "Misha", "Some body3", 5, FeedbackStatus.ON_REVIEW, null);

        List<Feedback> uncheckedFeedbacks = Arrays.asList(feedback1, feedback2, feedback3);

        BDDMockito.given(feedbackService.getAllUncheckedFeedbacks()).willReturn(uncheckedFeedbacks);

        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.get("/api/lib/feedback/onReview")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        BDDAssertions.then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        BDDAssertions.then(response.getContentAsString()).isEqualTo(jsonMultipleConverter.write(uncheckedFeedbacks).getJson());
    }
}
