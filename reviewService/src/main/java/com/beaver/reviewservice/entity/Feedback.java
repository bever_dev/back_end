package com.beaver.reviewservice.entity;

import com.beaver.reviewservice.FeedbackStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Feedback {
    @Id
    @GeneratedValue
    private Long feedbackId;
    private Long beverageId;
    private String userName;
    private String feedbackBody;
    private Integer feedbackRating;
    @Enumerated(EnumType.STRING)
    private FeedbackStatus feedbackStatus;
    private String profileImgUUID;
}
