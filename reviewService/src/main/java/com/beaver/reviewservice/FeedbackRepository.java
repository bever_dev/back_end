package com.beaver.reviewservice;

import com.beaver.reviewservice.entity.Feedback;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface FeedbackRepository extends CrudRepository<Feedback, Long> {
    default List<Feedback> getFeedbacksByFeedbackStatusIsOnReview() {
        return getFeedbacksByFeedbackStatus(FeedbackStatus.ON_REVIEW);
    }

    List<Feedback> getFeedbacksByFeedbackStatus(FeedbackStatus feedbackStatus);

    @Query("SELECT f FROM Feedback f WHERE f.beverageId=?1 AND f.feedbackStatus='CONFIRMED'")
    List<Feedback> getConfirmedFeedbacksByBeverageId(Long beverageId);

    @Transactional
    void deleteFeedbackByFeedbackId(Long feedbackId);

    @Transactional
    @Modifying
    @Query("UPDATE Feedback f SET f.feedbackStatus='CONFIRMED' WHERE f.feedbackId=?1")
    void updateStatusToConfirmedForFeedback(Long feedbackId);
}
