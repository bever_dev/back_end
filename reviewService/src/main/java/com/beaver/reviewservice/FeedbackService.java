package com.beaver.reviewservice;

import com.beaver.reviewservice.entity.Feedback;

import java.util.List;

public interface FeedbackService {
    Feedback addNewFeedbackForReview(Feedback feedback) throws IllegalArgumentException;

    void deleteFeedbackByFeedbackId(Long feedbackId) throws IllegalArgumentException;

    List<Feedback> getAllUncheckedFeedbacks();

    List<Feedback> getAllConfirmedFeedbacksForBeverage(Long beverageId);

    Feedback setFeedbackStatusIsConfirmed(Feedback feedback) throws IllegalArgumentException;
}
