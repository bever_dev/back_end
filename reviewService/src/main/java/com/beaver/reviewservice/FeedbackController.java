package com.beaver.reviewservice;

import com.beaver.reviewservice.entity.Feedback;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/lib/feedback")
public class FeedbackController {
    private final FeedbackService feedbackService;

    @PostMapping
    public ResponseEntity<Feedback> postFeedback(@RequestBody Feedback feedback) {
        Feedback addedFeedback = feedbackService.addNewFeedbackForReview(feedback);
        return ResponseEntity.ok(addedFeedback);
    }

    @GetMapping
    public ResponseEntity<List<Feedback>> getFeedbacksForBeverage(@RequestParam("beverageId") Long beverageId) {
        List<Feedback> confirmedFeedbacks = feedbackService.getAllConfirmedFeedbacksForBeverage(beverageId);
        return ResponseEntity.ok(confirmedFeedbacks);
    }

    @GetMapping("/onReview")
    public ResponseEntity<List<Feedback>> getFeedbacksOnReview() {
        List<Feedback> uncheckedFeedbacks = feedbackService.getAllUncheckedFeedbacks();
        return ResponseEntity.ok(uncheckedFeedbacks);
    }

    @PutMapping("/approve")
    public ResponseEntity<Feedback> approveFeedback(@RequestBody Feedback feedback) {
        Feedback approvedFeedback = feedbackService.setFeedbackStatusIsConfirmed(feedback);
        return ResponseEntity.ok(approvedFeedback);
    }

    @PostMapping("/delete")
    public ResponseEntity<Feedback> deleteFeedback(@RequestBody Feedback feedback) {
        feedbackService.deleteFeedbackByFeedbackId(feedback.getFeedbackId());
        return ResponseEntity.ok(feedback);
    }
}
