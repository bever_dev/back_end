package com.beaver.reviewservice;

public enum FeedbackStatus {
    ON_REVIEW,
    CONFIRMED
}
