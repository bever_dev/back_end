package com.beaver.reviewservice;

import com.beaver.reviewservice.entity.Feedback;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FeedbackServiceImpl implements FeedbackService {
    private final FeedbackRepository feedbackRepository;

    @Override
    public Feedback addNewFeedbackForReview(Feedback feedback) {
        if (feedback.getFeedbackBody().isEmpty()) {
            throw new IllegalArgumentException("Feedback body cannot be empty!");
        }
        if (feedback.getUserName().isEmpty()){
            throw new IllegalArgumentException("Feedback username cannot be empty!");
        }
        feedback.setFeedbackStatus(FeedbackStatus.ON_REVIEW);
        return feedbackRepository.save(feedback);
    }

    @Override
    public void deleteFeedbackByFeedbackId(Long feedbackId) {
        if (!feedbackRepository.existsById(feedbackId)){
            throw new IllegalArgumentException("Feedback with given id doesn't exist");
        }
        feedbackRepository.deleteFeedbackByFeedbackId(feedbackId);
    }

    @Override
    public List<Feedback> getAllUncheckedFeedbacks() {
        return feedbackRepository.getFeedbacksByFeedbackStatusIsOnReview();
    }

    @Override
    public List<Feedback> getAllConfirmedFeedbacksForBeverage(Long beverageId) {
        return feedbackRepository.getConfirmedFeedbacksByBeverageId(beverageId);
    }

    @Override
    public Feedback setFeedbackStatusIsConfirmed(Feedback feedback) {
        if (!feedbackRepository.existsById(feedback.getFeedbackId())){
            throw new IllegalArgumentException("Feedback with given id doesn't exist");
        }
        feedbackRepository.updateStatusToConfirmedForFeedback(feedback.getFeedbackId());
        feedback.setFeedbackStatus(FeedbackStatus.CONFIRMED);
        return feedback;
    }
}
