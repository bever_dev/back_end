package lib

import lib.repositories.BeverageRepository
import org.hibernate.boot.MetadataSources
import org.hibernate.boot.registry.{StandardServiceRegistry, StandardServiceRegistryBuilder}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.{CommandLineRunner, SpringApplication}
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

import java.io.File
import java.util.Collections
import javax.persistence.{EntityManager, EntityManagerFactory}

@SpringBootApplication
class App {
  @Autowired
  private var env: Environment = _

  def initStatic(): Unit = {
    //TODO move to beans and configurations
    val hibernateConfigKeyNameFromApplicationProperties = "hibernate.configuration"
    App.pathToHibernateXml = env.getProperty(hibernateConfigKeyNameFromApplicationProperties)

    App.registry = new StandardServiceRegistryBuilder()
      .configure(new File(App.hibernateCfgXml))
      .build()

    App.sessionFactory = new MetadataSources()
      .buildMetadata(App.registry)
      .buildSessionFactory()

    val tokenVerifierHostnameFromApplicationProperties = "api.security.hostname"
    val hostname = env.getProperty(tokenVerifierHostnameFromApplicationProperties)
    val tokenVerifierAPICallFromApplicationProperties = "api.security.auth.request"
    val apiEndPoint = env.getProperty(tokenVerifierAPICallFromApplicationProperties)
    App.authEndPoint = hostname + (if apiEndPoint(0) != '/' then "/" else "") + apiEndPoint
  }

  @Bean
  def run(repo: BeverageRepository): CommandLineRunner = new CommandLineRunner:
    override def run(args: String*): Unit = {
      initStatic()
    }
}

object App {
  var registry: StandardServiceRegistry = _
  var sessionFactory: EntityManagerFactory = _
  var pathToHibernateXml: String = _
  var authEndPoint: String = _

  def hibernateCfgXml: String = pathToHibernateXml

  def main(args: Array[String]): Unit = {
    val app = new SpringApplication(classOf[App])
    app.run(args.toList : _*)
  }
}
