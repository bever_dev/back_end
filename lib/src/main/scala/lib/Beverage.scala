package lib

import org.hibernate.annotations.GenericGenerator

import javax.persistence.{Column, Entity, GeneratedValue, GenerationType, Id, Table}
import scala.reflect.{ClassTag, classTag}


@Entity
@Table(name = Beverage.TABLE)
class Beverage() {
  @Id
  @Column(name = Beverage.COLUMN_ID)
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  var id: Int = _
  @Column(name = Beverage.COLUMN_NAME)
  var name: String = _
  @Column(name = Beverage.COLUMN_CREATOR)
  var creator: String = _
  @Column(name = Beverage.COLUMN_CATEGORY)
  var category: String = _
  @Column(name = Beverage.COLUMN_VOLUME)
  var volume: String = _
  @Column(name = Beverage.COLUMN_PERCENTAGE)
  var percentage: String = _
  @Column(name = Beverage.COLUMN_PRICE)
  var price: String = _
  @Column(name = Beverage.COLUMN_COMPOSITION)
  var composition: String = _
  @Column(name = Beverage.COLUMN_RATING)
  var rating: String = _
  @Column(name = Beverage.COLUMN_AVAILABLE)
  var available: String = _
  @Column(name = Beverage.COLUMN_IMAGE)
  var image: String = _
  
  def setId(id: Int): Unit = this.id = id
  def getId: Int = id

  def setName(name: String): Unit = this.name = name
  def getName: String = this.name

  def setCreator(creator: String): Unit = this.creator = creator
  def getCreator: String = creator

  def setCategory(category: String): Unit = this.category = category
  def getCategory: String = category

  def setVolume(volume: String): Unit = this.volume = volume
  def getVolume: String = volume

  def setPercentage(percentage: String): Unit = this.percentage = percentage
  def getPercentage: String = this.percentage

  def setPrice(price: String): Unit = this.price = price
  def getPrice: String = this.price

  def setComposition(composition: String): Unit = this.composition = composition
  def getComposition: String = composition

  def setRating(rating: String): Unit = this.rating = rating
  def getRating: String = rating

  def setAvailable(available: String): Unit = this.available = available
  def getAvailable: String = available

  def setImage(image: String): Unit = this.image = image
  def getImage: String = image

  override def toString: String = s"{$id, $creator}"
}

object Beverage {
  final val TABLE = "beverage"

  final val COLUMN_ID = "beverage_id" // Primary key
  final val COLUMN_NAME = "name"
  final val COLUMN_CREATOR = "creator_id" // Foreign key
  final val COLUMN_CATEGORY = "category"
  final val COLUMN_VOLUME = "volume"
  final val COLUMN_PERCENTAGE = "volume_percentage"
  final val COLUMN_PRICE = "price"
  final val COLUMN_COMPOSITION = "ingredients_composition"
  final val COLUMN_RATING = "rating"
  final val COLUMN_AVAILABLE = "available_number"
  final val COLUMN_IMAGE = "image_uuid"

  final val FIELD_ID = "id"
  final val FIELD_NAME = "name"
  final val FIELD_CREATOR = "creator"
  final val FIELD_CATEGORY = "category"
  final val FIELD_VOLUME = "volume"
  final val FIELD_PERCENTAGE = "percentage"
  final val FIELD_PRICE = "price"
  final val FIELD_COMPOSITION = "composition"
  final val FIELD_RATING = "rating"
  final val FIELD_AVAILABLE = "available"
  final val FIELD_IMAGE = "image"
}