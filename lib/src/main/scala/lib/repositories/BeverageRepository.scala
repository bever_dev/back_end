package lib.repositories

import lib.Beverage
import org.springframework.data.jpa.repository.{JpaRepository, JpaSpecificationExecutor}

trait BeverageRepository extends JpaRepository[Beverage, Int], JpaSpecificationExecutor[Beverage]