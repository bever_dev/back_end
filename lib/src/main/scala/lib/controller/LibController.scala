package lib.controller

import lib.Beverage.*
import lib.consumers.SecurityConsumer
import lib.controller.LibController.EMPTY_VALUE
import lib.{App, Beverage}
import lib.repositories.BeverageRepository
import lib.response.{LibResponse, LibResponsePage}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.{Page, PageRequest}
import org.springframework.data.jpa.domain.Specification
import org.springframework.web.bind.annotation.{GetMapping, PostMapping, RequestParam, RestController}

import javax.persistence.criteria.Predicate
import scala.jdk.CollectionConverters.*


@RestController
class LibController {
  @Autowired
  private var repo: BeverageRepository = _

  @Autowired
  private var auth: SecurityConsumer = _

  @GetMapping(value = Array("api/lib/beverages"))
  def getPage(@RequestParam(value = "number", defaultValue = "0") pageNumber: String,
              @RequestParam(value = "size",   defaultValue = "10") pageSize:   String): LibResponsePage = try {
    val page = pageNumber.toInt
    val size = pageSize.toInt
    val pageable = PageRequest.of(page, size)
    val result: Page[Beverage] = repo.findAll(pageable)
    LibResponsePage.onSuccess(result.getTotalPages, size, page, result.getContent.asScala.toArray)
  } catch {
    case _: Throwable => LibResponsePage.onFail
  }

  @GetMapping(value = Array("api/lib/beverage"))
  def get(@RequestParam(value = "id") id: String): LibResponse = try {
    val result = repo.findById(id.toInt).get()
    LibResponse.onSuccess(1, Array(result))
  } catch {
    case _: Throwable => LibResponse.onFail
  }

  //TODO: max remains for legacy compat
  @GetMapping(value = Array("api/lib/filter"))
  def filter(@RequestParam(value = FIELD_ID,           defaultValue = EMPTY_VALUE) id:            String,
             @RequestParam(value = FIELD_CREATOR,      defaultValue = EMPTY_VALUE) creator:       String,
             @RequestParam(value = FIELD_NAME,         defaultValue = EMPTY_VALUE) name:          String,
             @RequestParam(value = FIELD_CATEGORY,     defaultValue = EMPTY_VALUE) category:      String,
             @RequestParam(value = FIELD_VOLUME,       defaultValue = EMPTY_VALUE) volume:        String,
             @RequestParam(value = FIELD_PERCENTAGE,   defaultValue = EMPTY_VALUE) percentage:    String,
             @RequestParam(value = FIELD_PRICE,        defaultValue = EMPTY_VALUE) price:         String,
             @RequestParam(value = FIELD_COMPOSITION,  defaultValue = EMPTY_VALUE) composition:   String,
             @RequestParam(value = FIELD_RATING,       defaultValue = EMPTY_VALUE) rating:        String,
             @RequestParam(value = FIELD_AVAILABLE,    defaultValue = EMPTY_VALUE) available:     String,
             @RequestParam(value = FIELD_IMAGE,        defaultValue = EMPTY_VALUE) image:         String,
             @RequestParam(value = "number", defaultValue = "0") pageNumber: String,
             @RequestParam(value = "size",   defaultValue = "10") pageSize:   String,
             @RequestParam(value = "max", defaultValue = "10") maxNumber: String)
  : LibResponsePage = try {
    val page = pageNumber.toInt
    val size = pageSize.toInt
    val pageable = PageRequest.of(page, size)
    val spec = Specification
      .where(withParam(id, FIELD_ID))
      .and(withParam(creator, FIELD_CREATOR))
      .and(withParam(name, FIELD_NAME))
      .and(withParam(category, FIELD_CATEGORY))
      .and(withParam(volume, FIELD_VOLUME))
      .and(withParam(percentage, FIELD_PERCENTAGE))
      .and(withParam(price, FIELD_PRICE))
      .and(withParam(composition, FIELD_COMPOSITION))
      .and(withParam(rating, FIELD_RATING))
      .and(withParam(available, FIELD_AVAILABLE))
      .and(withParam(image, FIELD_IMAGE))
    val result: Page[Beverage] = repo.findAll(spec,pageable)
    LibResponsePage.onSuccess(result.getTotalPages, size, page, result.getContent.asScala.toArray)
  } catch {
    case _: Throwable => LibResponsePage.onFail
  }

  //TODO post
  @GetMapping(value = Array("api/lib/add"))
  def add(@RequestParam(value = "token",            defaultValue = EMPTY_VALUE) token:        String,
          @RequestParam(value = FIELD_CREATOR,      defaultValue = EMPTY_VALUE) creator:      String,
          @RequestParam(value = FIELD_NAME,         defaultValue = EMPTY_VALUE) name:         String,
          @RequestParam(value = FIELD_CATEGORY,     defaultValue = EMPTY_VALUE) category:     String,
          @RequestParam(value = FIELD_VOLUME,       defaultValue = EMPTY_VALUE) volume:       String,
          @RequestParam(value = FIELD_PERCENTAGE,   defaultValue = EMPTY_VALUE) percentage:   String,
          @RequestParam(value = FIELD_PRICE,        defaultValue = EMPTY_VALUE) price:        String,
          @RequestParam(value = FIELD_COMPOSITION,  defaultValue = EMPTY_VALUE) composition:  String,
          @RequestParam(value = FIELD_RATING,       defaultValue = EMPTY_VALUE) rating:       String,
          @RequestParam(value = FIELD_AVAILABLE,    defaultValue = EMPTY_VALUE) available:    String,
          @RequestParam(value = FIELD_IMAGE,        defaultValue = EMPTY_VALUE) image:        String)
  : LibResponse = try {
    //TODO check token
    val beverage = new Beverage()

    if (creator     != EMPTY_VALUE) beverage.setCreator(creator)
    if (name        != EMPTY_VALUE) beverage.setName(name)
    if (category    != EMPTY_VALUE) beverage.setCategory(category)
    if (volume      != EMPTY_VALUE) beverage.setVolume(volume)
    if (percentage  != EMPTY_VALUE) beverage.setPercentage(percentage)
    if (price       != EMPTY_VALUE) beverage.setPrice(price)
    if (composition != EMPTY_VALUE) beverage.setComposition(composition)
    if (rating      != EMPTY_VALUE) beverage.setRating(rating)
    if (available   != EMPTY_VALUE) beverage.setAvailable(available)
    if (image       != EMPTY_VALUE) beverage.setImage(image)

    LibResponse.onSuccess(1, Array(repo.save(beverage)))
  } catch {
    case _: Throwable => LibResponse.onFail
  }
}

object LibController {
  final val EMPTY_VALUE = "-"
}

def withParam(value:String, fieldName:String): Specification[Beverage] =
  (root, query, builder) => if(value == EMPTY_VALUE) builder.conjunction() else builder.equal(root.get(fieldName), value)

