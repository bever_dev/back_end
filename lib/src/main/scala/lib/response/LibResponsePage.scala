package lib.response

import lib.Beverage
import lib.response.LibResponse.{FAIL, SUCCESS}


/**
 * Represents response-object of the lib-service
 * 
 * @param status 0 or 1 corresponding to SUCCESS or FAIL
 * @param pages total number of pages with specified size
 * @param size requested size of the page
 * @param page zero-based number of page
 * @param length number of objects returned in this page
 * @param beverage array of returned objects
 */
class LibResponsePage(var status: Int, var pages: Int, var size: Int,
                      var page: Int, var length: Int, var beverage: Array[Beverage]) {
  def setStatus(status: Int): Unit = this.status = status
  def getStatus: Int = status

  def setPages(pages: Int): Unit = this.pages = pages
  def getPages: Int = this.pages
  
  def setPage(page: Int): Unit = this.page = page
  def getPage: Int = this.page
  
  def setSize(size: Int): Unit = this.size = size
  def getSize: Int = this.size

  def setLength(length: Int): Unit = this.length = length
  def getLength: Int = this.length
  
  def setBeverage(beverage: Array[Beverage]): Unit = this.beverage = beverage
  def getBeverage: Array[Beverage] = this.beverage
}

object LibResponsePage extends Enumeration {
  def onSuccess(pages: Int, size: Int, page: Int, beverages: Array[Beverage]) = 
    new LibResponsePage(SUCCESS, pages, size, page, beverages.length, beverages)
  def onSuccess: LibResponsePage = new LibResponsePage(SUCCESS, 0, 0, 0, 0, Array.empty[Beverage])
  def onFail: LibResponsePage = new LibResponsePage(FAIL, 0, 0, 0, 0, Array.empty[Beverage])
}
