package lib.response

import lib.Beverage

/**
 * Represents response-object of the lib-service
 *
 * @param status 0 or 1 corresponding to SUCCESS or FAIL
 * @param length number of objects returned in this page
 * @param beverage array of returned objects
 */
class LibResponse(var status: Int, var length: Int, var beverage: Array[Beverage]) {
  def setBeverage(beverage: Array[Beverage]): Unit = this.beverage = beverage
  def getBeverage: Array[Beverage] = this.beverage

  def setSize(size: Int): Unit = this.length = size
  def getSize: Int = this.length
  
  def setStatus(status: Int): Unit = this.status = status
  def getStatus: Int = status
}

object LibResponse extends Enumeration {
  final val SUCCESS = 0
  final val FAIL = 1

  def onSuccess(size: Int, beverages: Array[Beverage]): LibResponse = new LibResponse(SUCCESS, size, beverages)
  def onSuccess: LibResponse = new LibResponse(SUCCESS, 0, Array.empty[Beverage])
  def onFail: LibResponse = new LibResponse(FAIL, 0, Array.empty[Beverage])
}
