package lib.consumers

import lib.App
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment

@Bean
class SecurityConsumer {
  @Autowired
  private var builder: RestTemplateBuilder = _

  //TODO move from parameter to header
  def checkToken(token: String): Boolean = try {
    false
  } catch {
    case _: Throwable => false
  }
}