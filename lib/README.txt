# Build

1) In root folder run ./gradlew build (if can't, then try "chmod 700 gradlew")
2) After build complete go to build/resources folder and copy application.properties and cfg.xml to build/libs folder
3) Check that you have 2 jars in build/libs: with -plain postfix and without, you need one without
4) Run that jar-file with "java -jar lib-file.jar"


# Configuration

1) You can configure keys spring.port and hibernate.configuration in application.properties
    to change service hosting port or change hibernate configuration file name
2) To change target data-base credentials and url (postgres only) change *.url, *.username and *.password to desired
    both in application.properties and hibernate.configuration file
3) To switch to release-working-mode change spring.jpa.hibernate.ddl-auto to update in application.properties
4) You can also change both: api.security.hostname and api.security.auth.request to specify custom server and
    request name of token-validation (used in add api call)