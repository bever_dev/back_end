pipeline {
    agent any

    tools {
        dockerTool 'bever-docker'
        jdk 'openjdk-17'
    }

    stages {

        stage('Build artefact') {
            parallel {
                stage('Build lib'){
                    steps{
                        updateGitlabCommitStatus name: 'pipeline', state: 'running'
                        dir("lib") {
                            sh "./gradlew -g /var/gradle-cache build"
                        }
                    }
                }
                stage('Build auth'){
                    steps{
                        dir("auth") {
                            sh "./gradlew -g /var/gradle-cache build"
                        }
                    }
                }
                stage('Build rev'){
                    steps{
                        dir("reviewService") {
                            sh "./gradlew -g /var/gradle-cache build"
                        }
                    }
                }
                stage('Build pref'){
                    steps{
                        dir("prefer") {
                            sh "./gradlew -g /var/gradle-cache build"
                        }
                    }
                }
            }
        }

        stage('SonarQube Analysis') {
            parallel {
                stage('Sonar lib'){
                    environment {
                        sonar = tool 'sonarqube-4.7.0'
                    }

                    steps {
                        dir('lib'){
                            withSonarQubeEnv(installationName: 'bever-sonarcloud', credentialsId: 'sonarcloud-token') {
                                sh '''${sonar}/bin/sonar-scanner \
                                -Dsonar.projectKey=bever-lib  \
                                -Dsonar.coverage.exclusions="src/main/resources/**/*.*, **/*Test*" '''
                            }
                        }
                    }
                }
                stage('Sonar auth'){
                    environment {
                        sonar = tool 'sonarqube-4.7.0'
                    }

                    steps {
                        dir('auth'){
                            withSonarQubeEnv(installationName: 'bever-sonarcloud', credentialsId: 'sonarcloud-token') {
                                sh '''${sonar}/bin/sonar-scanner \
                                -Dsonar.java.binaries=build/classes \
                                -Dsonar.projectKey=bever-auth \
                                -Dsonar.coverage.exclusions="src/test/**/*.*, src/main/resources/**/*.*, **/*Test*" \
                                -Dsonar.coverage.jacoco.xmlReportPaths=build/reports/jacoco/test/jacocoTestReport.xml '''
                            }
                        }
                    }
                }

                stage('Sonar rev'){
                    environment {
                        sonar = tool 'sonarqube-4.7.0'
                    }

                    steps {
                        dir('reviewService'){
                            withSonarQubeEnv(installationName: 'bever-sonarcloud', credentialsId: 'sonarcloud-token') {
                                sh '''${sonar}/bin/sonar-scanner \
                                -Dsonar.java.binaries=build/classes \
                                -Dsonar.projectKey=bever-rev \
                                -Dsonar.coverage.exclusions="src/test/**/*.*, **/src/main/resources/**/*.*, **/*Test*" \
                                -Dsonar.coverage.jacoco.xmlReportPaths=build/reports/jacoco/test/jacocoTestReport.xml '''
                            }
                        }
                    }
                }

                stage('Sonar pref'){
                    environment {
                        sonar = tool 'sonarqube-4.7.0'
                    }

                    steps {
                        dir('prefer'){
                            withSonarQubeEnv(installationName: 'bever-sonarcloud', credentialsId: 'sonarcloud-token') {
                                sh '''${sonar}/bin/sonar-scanner \
                                -Dsonar.java.binaries=build/classes \
                                -Dsonar.projectKey=bever-pref \
                                -Dsonar.coverage.exclusions="src/test/**/*.*, **/src/main/resources/**/*.*, **/*Test*" \
                                -Dsonar.coverage.jacoco.xmlReportPaths=build/reports/jacoco/test/jacocoTestReport.xml '''
                            }
                        }
                    }
                }
            }
        }

        stage('Build image') {
            parallel{
                stage('Build lib image'){
                    steps{
                        dir("lib"){
                            sh "docker build . -t ultimatehikari/bever-lib:1.${env.BUILD_NUMBER}"
                        }
                    }
                }
                stage('Build auth image'){
                    steps{
                        dir("auth"){
                            sh "docker build . -t ultimatehikari/bever-auth:1.${env.BUILD_NUMBER}"
                        }
                    }
                }
                stage('Build rev image'){
                    steps{
                        dir("reviewService"){
                            sh "docker build . -t ultimatehikari/bever-rev:1.${env.BUILD_NUMBER}"
                        }
                    }
                }
                stage('Build pref image'){
                    steps{
                        dir("prefer"){
                            sh "docker build . -t ultimatehikari/bever-pref:1.${env.BUILD_NUMBER}"
                        }
                    }
                }
            }
        }

        stage('Push image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'dockerhub-password', passwordVariable: 'password', usernameVariable: 'user')]){
                    sh "docker login -u ${env.user} -p ${env.password}"
                    sh "docker push ultimatehikari/bever-lib:1.${env.BUILD_NUMBER}"
                    sh "docker push ultimatehikari/bever-auth:1.${env.BUILD_NUMBER}"
                    sh "docker push ultimatehikari/bever-rev:1.${env.BUILD_NUMBER}"
                    sh "docker push ultimatehikari/bever-pref:1.${env.BUILD_NUMBER}"
                }
            }
        }

        stage('Kubenetes deploy') {
            steps {
                withCredentials([file(credentialsId: 'kubeconfig', variable: 'CONFIG')]) {
                    sh "kubectl set image deployment/auth reg-pod=ultimatehikari/bever-auth:1.${env.BUILD_NUMBER} --kubeconfig=\"$CONFIG\""
                    sh "kubectl set image deployment/lib lib-pod=ultimatehikari/bever-lib:1.${env.BUILD_NUMBER} --kubeconfig=\"$CONFIG\""
                    sh "kubectl set image deployment/rev rev-pod=ultimatehikari/bever-rev:1.${env.BUILD_NUMBER} --kubeconfig=\"$CONFIG\""
                    sh "kubectl set image deployment/pref pref-pod=ultimatehikari/bever-pref:1.${env.BUILD_NUMBER} --kubeconfig=\"$CONFIG\""
                }
            }
        }
    }

    post {
        failure {
            updateGitlabCommitStatus name: 'pipeline', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'pipeline', state: 'success'
        }
        aborted {
            updateGitlabCommitStatus name: 'pipeline', state: 'canceled'
        }
    }
}